import cls from "./Button.module.css";
import { ButtonProps } from "./Button.props";
import cn  from "classnames";
import Arrow from "@/public/Arrow.svg";
import { JSX } from "react/jsx-runtime";

export default function Button({appearance, children, arrow, className, ...props }: ButtonProps): JSX.Element {
  return(
    <button className={cn(cls.button, className, {
        [cls.primary]: appearance == 'primary',
        [cls.ghost]: appearance == 'ghost'
    }
    )}
        {...props}
    >
        {children}
        {arrow !== 'none' &&
        <Arrow className={cn(cls.arrow,{
            [cls.down]: arrow == 'down',
            [cls.right]: arrow == 'right'
        })}/>
        }
    </button>
  );
}