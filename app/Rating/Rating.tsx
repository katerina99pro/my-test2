'use client';

import { RatingProps } from './Rating.props';
import Star from '@/public/star.svg';
import cls from './Rating.module.css';
import cn from 'classnames';
import { useEffect, useState, KeyboardEvent } from 'react';

export default function Rating({
  rating,
  setRating,
  isEditable = false,
  ...props
}: RatingProps): JSX.Element {
  const [ratingArray, setRatingArray] = useState<JSX.Element[]>(
    new Array(5).fill(<></>)
  );

  useEffect(() => {
    constructRating(rating);
  }, [rating]);

  // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
  const constructRating = (currentRating: number) => {
    const updatedArray = ratingArray.map((r: JSX.Element, i: number) => {
      return (
        <span
          className={cn(cls.star, {
            [cls.filled]: i < currentRating,
            [cls.editabled]: isEditable,
          })}
          onMouseEnter={() => changeDisplay(i + 1)}
          onMouseLeave={() => changeDisplay(rating)}
          onClick={() => onClick(i+1)}
        >
          <Star tabIndex={isEditable ? 0 :-1}
          onKeyDown={(e: KeyboardEvent<SVGAElement>) => isEditable && handleSpace(i + 1, e)}/>
        </span>
      );
    });
    setRatingArray(updatedArray);
  };
  // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
  const changeDisplay = (i: number) => {
    if (!isEditable) {
      return;
    }
    constructRating(i);
  };
  // eslint-disable-next-line @typescript-eslint/explicit-function-return-type, @typescript-eslint/no-unused-vars
  const onClick = (i: number) => {
    if (!isEditable || !setRating) {
      return;
    }
    setRating(i);
  };
  // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
  const handleSpace = (i: number, e: KeyboardEvent<SVGAElement>) => {
    if (e.code !='Space' || !setRating) {
      return;
    }
    setRating(i);
  };
  return (
    <div {...props}>
      {ratingArray.map((r, i) => (
        <span key={i}>{r}</span>
      ))}
    </div>
  );
}

