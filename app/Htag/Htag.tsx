import { HtagProps } from "./Htag.props";
import cls from "./Htag.module.css";

export default function Htag({ tag, children }: HtagProps): JSX.Element {
  switch (tag) {
    case 'h1':
      return <h1 className={cls.h1}>{children}</h1>;
    case 'h2':
      return <h2 className={cls.h2}>{children}</h2>;
    case 'h3':
      return <h3 className={cls.h3}>{children}</h3>;
    default:
      return <></> ;
  }
  
}
