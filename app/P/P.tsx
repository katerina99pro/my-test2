import { PProps } from './P.props';
import cls from './P.module.css';
import cn from 'classnames';

export default function P({ size = 'm', children, className, ...props }: PProps): JSX.Element {
  return (
    <>
      <p
        className={cn(cls.default, className, {
          [cls.s]: size == 's',
          [cls.m]: size == 'm',
          [cls.xl]: size == 'xl',
        })}
        {...props}
      >
        {children}
      </p>
    </>
  );
}
